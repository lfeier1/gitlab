# gitlab

## Getting started

Install the GitLab CLI tool ([glab](https://docs.gitlab.com/ee/integration/glab/))

``` shell
brew install glab
```

Install additional tools used to process the responses:

``` shell
brew install jq
brew install csvkit
```

Create an access token using the [Access Tokens](https://gitlab.com/-/profile/personal_access_tokens) web interface. Login:
``` shell
glab auth login
```
## Sample requests
Get all GitLab contributions in a time range:
```
glab api graphql --paginate --raw-field fromDate=20230101 --raw-field toDate=20230110 --field query="@queries/contributions.graphql"
```
Notice the ``--paginate`` flag used to make additional requests and fetch all pages of results.

Get contributions in ``csv`` format:
```
glab api graphql --paginate --raw-field fromDate=20221201 --raw-field toDate=20221231 --field query="@queries/contributions.graphql" | \
  jq -r '["username","repoPushed","mergeRequestsCreated","mergeRequestsApproved","mergeRequestsClosed","totalEvents"], (.data.group.contributions.nodes[] | [.user.username, .repoPushed, .mergeRequestsCreated, .mergeRequestsApproved, .mergeRequestsClosed, .totalEvents]) | @csv'
```
Sample output:

``` shell
"username","repoPushed","mergeRequestsCreated","mergeRequestsApproved","mergeRequestsClosed","totalEvents"
"aarondelaplane",117,21,27,4,185
"idriver_wh",175,25,13,0,235
"cwdesautels",132,18,16,3,187
...
```
Get all **caesarsdigital** group members:
``` shell
glab api --paginate "groups/12568027/members"
glab api graphql --paginate --field query="@queries/members.graphql"
```
Get members in ``csv`` format:
``` shell
glab api graphql --paginate --field query="@queries/members.graphql" | \
  jq -r '["username","name","accessLevel","createdBy","createdAt","expiresAt","createProjects","createProjects"], (.data.group.groupMembers.nodes[] | [.user.username, .user.name, .accessLevel.stringValue, .createdBy.username, .createdAt, .expiresAt, .userPermissions.createProjects, .userPermissions.readGroup]) | @csv'
```
Sample output:

``` shell
"username","name","accessLevel","createdBy","createdAt","expiresAt","createProjects","createProjects"
"LQuinn1","Luke Quinn","GUEST",,"2022-11-23T18:21:38Z",,true,true
"dwallin","Dave Wallin","GUEST",,"2022-11-02T18:21:36Z",,true,true
"dmeredith1","Drew Meredith","GUEST",,"2022-10-25T21:53:16Z",,true,true
"jsha11","Jonathan Sha","GUEST",,"2022-10-12T18:54:22Z",,true,true
...
```
