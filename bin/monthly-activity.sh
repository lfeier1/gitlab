#!/usr/bin/env sh
# TODO: calculate the time ranges

MONTHS=('Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec')

DATE_RANGES=(
    '20220101,20220131' \
    '20220201,20220228' \
    '20220301,20220331' \
    '20220401,20220430' \
    '20220501,20220531' \
    '20220601,20220630' \
    '20220701,20220731' \
    '20220801,20220831' \
    '20220901,20220930' \
    '20221001,20221031' \
    '20221101,20221130' \
    '20221201,20221231' \
    )

TMP_DIR=$(dirname $0)/../tmp

MEMBERS_FILE=$TMP_DIR/members.csv
MEMBER_CONTRIBUTIONS_FILE=$TMP_DIR/member-contributions.csv

mkdir -p $TMP_DIR

if [ ! -f "$MEMBERS_FILE" ]
then
    glab api graphql --paginate --field query="@queries/members.graphql" | \
         jq -r '["username","name","accessLevel","createdBy","createdAt","expiresAt","createProjects","readGroup"], (.data.group.groupMembers.nodes[] | [.user.username, .user.name, .accessLevel.stringValue, .createdBy.username, .createdAt, .expiresAt, .userPermissions.createProjects, .userPermissions.readGroup]) | @csv' \
         > $MEMBERS_FILE

    HEADER=$(head -1 $MEMBERS_FILE)
    gsed -i '/username/d' $MEMBERS_FILE
    gsed -i "1 i $HEADER" $MEMBERS_FILE
fi

cp $MEMBERS_FILE $MEMBER_CONTRIBUTIONS_FILE

for RANGE in "${DATE_RANGES[@]}"
do
    IFS="," read -r -a DATES <<< "${RANGE}"

    echo Processing ${MONTHS[$((10#${DATES[0]:4:2}-1))]}...

    CM=${MONTHS[$((10#${DATES[0]:4:2}-1))]}
    CONTRIBUTIONS_FILE=$TMP_DIR/contributions-${DATES[0]}-${DATES[1]}.csv

    if [ ! -f "$CONTRIBUTIONS_FILE" ]
    then
        glab api graphql --paginate --raw-field fromDate=${DATES[0]} --raw-field toDate=${DATES[1]} --field query="@queries/contributions.graphql" | \
            jq -r "[\"username\",\"repoPushed${CM}\",\"totalEvents${CM}\"], (.data.group.contributions.nodes[] | [.user.username, .repoPushed, .totalEvents]) | @csv" \
            > $CONTRIBUTIONS_FILE

        HEADER=$(head -1 $CONTRIBUTIONS_FILE)
        gsed -i '/username/d' $CONTRIBUTIONS_FILE
        gsed -i "1 i $HEADER" $CONTRIBUTIONS_FILE
    fi

    csvjoin -c username -d ',' --left $MEMBER_CONTRIBUTIONS_FILE $CONTRIBUTIONS_FILE > "${MEMBER_CONTRIBUTIONS_FILE}.tmp"
    mv "${MEMBER_CONTRIBUTIONS_FILE}.tmp" ${MEMBER_CONTRIBUTIONS_FILE}
done
